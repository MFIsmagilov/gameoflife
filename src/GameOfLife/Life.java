/**
 * Created by MaRaT on 01.03.2016.
 */
package GameOfLife;
import java.util.Random;

public class Life {

    protected int N;
    protected int M;
    protected int countLive;

    public void setN(int n)
    {
        N = n;
    }

    public void setM(int m)
    {
        M = m;
    }

    public void setCountLive(int countLive)
    {
        this.countLive = countLive;
    }

    protected int[][] field;

    private int EMPTY = 0;
    private int ALIVE = 1;
    private int GONNA_DIE = 2;
    private int GONNA_LIVE = 4;

    private int UPPER_LIVE_BORDER = 3;
    private int LOWER_LIVE_BORDER = 2;

    private int BORN_BORDER = 3;

    protected  Life(){

    }
    public Life(int N, int M, int countLive)
    {
        this.N = N;
        this.M = M;
        this.countLive = countLive;
        this.field = new int[N][M];
        Random rnd = new Random();
        for (int i = 0; i < countLive; ) {
            int row = rnd.nextInt(N);
            int col = rnd.nextInt(M);
            if (field[row][col] == 0) {
                field[row][col] = 1;
                i++;
            }
        }
    }

    public Life(int N, int M)
    {
        this.N = N;
        this.M = M;
        this.field = new int[N][M];
        this.countLive=0;
    }

    public int getValue(int i, int j)
    {
        return field[i][j];
    }

    public void setValue(int i, int j, int life)
    {
        if(field[i][j]==1 && life==0) countLive--;
        if(field[i][j]==0 && life==1) countLive++;

        field[i][j] = life;
    }
    private boolean isCenter(int x, int y, int i, int j)
    {
        return (x == i) && (y == j);
    }

    private boolean isOutBorder(int x, int y)
    {
        return (x >= 0 && x < N) && (y >= 0 && y < M);
    }


    private int countNeighbors(int i, int j)
    {
        int count = 0;
        for (int x = i - 1; x <= i + 1; x++) {
            for (int y = j - 1; y <= j + 1; y++) {
                if (!isCenter(x, y, i, j) && isOutBorder(x,y) && field[x][y] == 1)
                    count++;
            }
        }
        return count;
    }

    public void step()
    {
        int[][] tempField = new int[N][M];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                int countNeighbors = countNeighbors(i, j);
                if (field[i][j] == 0 && countNeighbors == 3) {
                    tempField[i][j] = 1;
                    countLive++;
                } else{
                    if (field[i][j] == 1 && (countNeighbors == 2 || countNeighbors == 3)) {
                        tempField[i][j] = 1;
                    }
                    else
                        if(field[i][j]==1 && (countNeighbors < 2 || countNeighbors > 3))
                            countLive--;
                }
            }
        }
        field = tempField;
    }

    public int get_value(int x, int y){
        if(x >= 0 && y >= 0 && x < field.length && y < field.length) {
            return field[x][y];
        }
        else {
            return 0;
        }
    }


    //...
    public void step1(){
        for(int i = 0; i < N; i++){
            for(int j = 0; j < M; j++){
                int sum = 0;
                for(int k = 0; k < 9; k++){
                    if((get_value(i - 1 + k / 3, j - 1 + k % 3) & (ALIVE | GONNA_DIE)) > 0 && k != 4){
                        sum++;
                    }
                }
                if(field[i][j] == ALIVE && (sum < LOWER_LIVE_BORDER || sum > UPPER_LIVE_BORDER)){
                    field[i][j] = GONNA_DIE;
                }
                else
                if(field[i][j] == EMPTY &&  sum == BORN_BORDER){
                    field[i][j] = GONNA_LIVE;
                }
            }
        }
        for(int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                field[i][j] = (field[i][j] & (GONNA_LIVE | ALIVE)) % 3;
            }
        }
    }

    public  int getCountLive()
    {
        return countLive;
    }
    public int getM()
    {
        return M;
    }

    public int getN()
    {
        return N;
    }

}
