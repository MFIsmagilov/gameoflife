package CameOfLifeSwing;

//import GameOfLife.Life;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by MaRaT on 02.03.2016.
 */
public class Window {
    private JFrame jf;
    private Container container;
    private Life life;
    private JPanelMy[][] cellsJPanelMy;
    private JMenuBar menuBar;
    private StatusBar statusBar;

    private int width = 500;
    private int height = 500;
    private int N;
    private int M;
    private int countGeneration;
    private int speed = 100;

    private boolean flag = true;

    public Window(int width, int height)
    {
        this.life = new Life();
        //this.life.loadFromDataBase();
        ConstructorWindows(width, height, life.getN(), life.getM());
    }
    public Window(int width, int height, int N, int M)
    {
        this.life = new Life(N, M);
        ConstructorWindows(width, height, N, M);
    }

    public Window(int width, int height, int N, int M, int countLife)
    {
        this.life = new Life(N, M, countLife);
        ConstructorWindows(width, height, N, M);
    }

    private void ConstructorWindows(int width, int height, int N, int M)
    {
        this.width = width;
        this.height = height;
        this.N = N;
        this.M = M;
        this.cellsJPanelMy = new JPanelMy[N][M];
        this.countGeneration = 0;
        jf = new JFrame("Game of Life");
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setSize(this.width, this.height);
        jf.setLocationRelativeTo(null);
        container = jf.getContentPane();
        JPanel jPanel = new JPanel();
        JScrollPane jScrollPane = new JScrollPane(jPanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        Dimension sizeMonitor = Toolkit.getDefaultToolkit().getScreenSize();
        jPanel.setSize(sizeMonitor.width, sizeMonitor.height);
        jPanel.setLayout(new GridLayout(N, M, 1, 1));
        jPanel.setBackground(Color.BLUE);
        container.add(jScrollPane);


        for (int i = 0; i < N; i++)
            for (int j = 0; j < M; j++) {
                cellsJPanelMy[i][j] = new JPanelMy(life.getValue(i, j));
                jPanel.add(cellsJPanelMy[i][j]);
            }
        addJMenuBar();
        statusBar = new StatusBar();
        statusBar.setText("Generation: 0");
        container.add(statusBar, java.awt.BorderLayout.SOUTH);
    }

    private void addJMenuBar()
    {
        Font font = new Font("Verdana", Font.PLAIN, 12);

        menuBar = new JMenuBar();

        JMenu settingsJMenu = new JMenu("Настройки");
        settingsJMenu.setFont(font);

        JMenu startJMenu = new JMenu("Старт");
        startJMenu.setFont(font);
        startJMenu.addMouseListener(new MouseListenerStart());

        JMenu stopJMenu = new JMenu("Пауза");
        stopJMenu.setFont(font);
        stopJMenu.addMouseListener(new MouseListenerPause());

        JMenu saveJMenu = new JMenu("Сохранить");
        saveJMenu.setFont(font);
        saveJMenu.addMouseListener(new MouseListenerSave());

        JMenu exitJMenu = new JMenu("Выход");
        exitJMenu.setFont(font);
        exitJMenu.addMouseListener(new MouseListenerExit());

        JMenu newMenu = new JMenu("Поле");
        newMenu.setFont(font);
        settingsJMenu.add(newMenu);

        JMenu clearField = new JMenu("Очистить");
        clearField.setFont(font);
        clearField.addMouseListener(new MouseListenerClear());

//        JMenuItem sizeItem0 = new JMenuItem("60x40");
//        sizeItem0.setFont(font);
//        //sizeItem0.addMouseListener(new MouseListenerSize(60,40));
//        newMenu.add(sizeItem0);
//
//        JMenuItem sizeItem1 = new JMenuItem("80x60");
//        sizeItem1.setFont(font);
//        //sizeItem1.addMouseListener(new MouseListenerSize(80,60));
//        newMenu.add(sizeItem1);
//
//        JMenuItem sizeItem2 = new JMenuItem("120x100");
//        sizeItem2.setFont(font);
//        //sizeItem1.addMouseListener(new MouseListenerSize(120,100));
//        newMenu.add(sizeItem2);

        menuBar.add(startJMenu);
        menuBar.add(stopJMenu);
        menuBar.add(saveJMenu);
        menuBar.add(clearField);
        menuBar.add(Box.createHorizontalGlue());
        menuBar.add(exitJMenu);

        jf.setJMenuBar(menuBar);
        jf.setPreferredSize(new Dimension(this.width, this.height));
        jf.pack();
        jf.setLocationRelativeTo(null);
    }

    public void startGame()
    {
        for (int i = 0; i < N; i++)
            for (int j = 0; j < M; j++) {
                this.life.setValue(i, j, cellsJPanelMy[i][j].getLife());
            }
        final Timer timer = new Timer(speed, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (flag) {
                    life.step();
                    for (int i = 0; i < N; i++)
                        for (int j = 0; j < M; j++) {
                            cellsJPanelMy[i][j].setLife(life.getValue(i, j));
                        }
                    countGeneration++;
                    statusBar.setText(" Generation : " + countGeneration);
                }
            }
        });
        timer.start();
    }

    public void pauseGame()
    {
        //this.flag = flag ? false : true;
        JMenu jMenu = menuBar.getMenu(1);
        if (flag) {
            flag = false;
            jMenu.setText("Продолжить");
        } else {
            flag = true;
            jMenu.setText("Пауза");
        }
    }

    public void setVisible(boolean b)
    {
        jf.setVisible(b);
    }


    public class MouseListenerSave extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            int countLive = 0;
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++) {
                    int life = cellsJPanelMy[i][j].getLife();
                    if(life == 1) {
                        Window.this.life.setValue(i, j, cellsJPanelMy[i][j].getLife());
                        countLive++;
                    }
                }
            life.setCountLive(countLive);
            Window.this.life.saveInDataBase();
        }
    }

    public class MouseListenerClear extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            for (int i = 0; i < N; i++)
                for(int j = 0; j < M; j++) {
                    Window.this.life.setValue(i,j,0);
                    Window.this.cellsJPanelMy[i][j].setLife(0);
                }
        }
    }

    public class MouseListenerSize extends MouseAdapter {

        public MouseListenerSize(int width, int height)
        {
            Window.this.width = width;
            Window.this.height = height;
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
            jf.setSize(width, height);
        }
    }

    public class MouseListenerStart extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            startGame();
        }
    }

    public class MouseListenerPause extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            pauseGame();
        }
    }

    public class MouseListenerExit extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e)
        {
            System.exit(0);
        }
    }
}
