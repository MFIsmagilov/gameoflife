package CameOfLifeSwing;

import java.sql.*;

/**
 * Created by MaRaT on 30.03.2016.
 */
//Game of life Save parametr in DataBase
public class Life extends GameOfLife.Life {
    private Connection con;
    private Statement com;

    private String nameTable = "coor";
    private String sizeTable = "sizefield";

    private String url = "jdbc:mysql://localhost:3306/gameoflife";
    private String user = "root";
    private String password = "root";

    public Life()
    {
        ConnectBD();
        this.loadFromDataBase();
    }

    public Life(int N, int M)
    {
        super(N, M);
        ConnectBD();
    }

    public Life(int N, int M, int countLive)
    {
        super(N, M, countLive);
        ConnectBD();
    }

    public Life(int N, int M, int countLive, String connectionString)
    {
        super(N, M, countLive);
        try {
            con = DriverManager.getConnection(connectionString);
            com = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void ConnectBD()
    {
        try {
            con = DriverManager.getConnection(url, user, password);
            com = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void clearDataBase()
    {
        try {
            this.com.executeUpdate("DELETE FROM " + this.nameTable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveInDataBase()
    {
        //clearDataBase();
        try {
            this.com.executeUpdate("DELETE FROM " + this.sizeTable);
            this.com.executeUpdate("DELETE FROM " + this.nameTable);

            this.com.executeUpdate("INSERT INTO " + this.sizeTable +
                                            " (N,M,count) VALUES(" +
                                            this.N + "," +
                                            this.M + "," +
                                            this.countLive + ")");
            for (int i = 0; i < super.N; i++)
                for (int j = 0; j < super.M; j++)
                    if(field[i][j] == 1)
                        this.com.executeUpdate("INSERT INTO " +
                                this.nameTable + " (i,j,life) VALUES(" + i + "," + j + "," + field[i][j] + ");");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void loadFromDataBase()
    {

        try {
            ResultSet size = this.com.executeQuery("SELECT * FROM " + this.sizeTable);
            System.out.println(size);
            size.next();
            this.N = size.getInt("n");
            this.M = size.getInt("m");
            this.countLive = size.getInt("count");
            this.field = new int[this.N][this.M];
            size.close();
            ResultSet res = this.com.executeQuery("SELECT * FROM " + this.nameTable);
            while (res.next()) {
                int i = res.getInt("i");
                int j = res.getInt("j");
                int life = res.getInt("life");
                this.field[i][j] = life;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
