package CameOfLifeSwing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by MaRaT on 03.03.2016.
 */
public class JPanelMy extends JPanel {
    private int life = 0;
    private boolean flag = false;

    public JPanelMy(int life)
    {
        this.life = life;
        //this.setBackground(Color.black);
        addMouseListener(new MouseListenerCell());
        this.setBackground(Color.BLUE);
    }

    public void setLife(int life)
    {
        this.life = life;
        repaint();
    }

    public int getLife()
    {
        return life;
    }

    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        //int w = getWidth()>getHeight()?getHeight():getWidth();
        if (life == 0) {
            //this.setBackground(Color.BLACK);
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, 20, 20);
        } else {
            g.setColor(Color.green);
            g.fillRect(0, 0, 20, 20);
        }
    }


    public class MouseListenerCell extends MouseAdapter {


        @Override
        public void mouseClicked(MouseEvent e)
        {
            life = life == 0 ? 1 : 0;
            JPanelMy.this.repaint();
        }

        //-_-
//        @Override
//        public void mouseEntered(MouseEvent e)
//        {
//            System.out.println("Entered : " + e.getX()+","+e.getY() + flag);
//
//            if(flag == true) {
//                life = 1;
//                JPanelMy.this.repaint();
//            }
//        }
//
//        @Override
//        public void mousePressed(MouseEvent e) {
//            System.out.println("Pressed : " + e.getX()+","+e.getY() + flag);
//            flag = true;
//        }
//
//        @Override
//        public void mouseReleased(MouseEvent e) {
//        }
    }
}
