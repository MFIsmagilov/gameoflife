package GameOfLifeServer;

import javax.swing.*;
import java.awt.*;

/**
 * Created by MaRaT on 25.03.2016.
 */
public class StatusBar extends JLabel {

    public StatusBar() {
        super();
        super.setPreferredSize(new Dimension(100, 16));}

    public void setMessage(String message) {
        setText(" " + message);
    }

    public void setMessage(int message) {
        setText(" " + Integer.toString(message));
    }

}